pragma solidity ^0.4.16;

/// @title Voting with delegation.
contract SellEventTickets {
    // This declares a new complex type which will
    // be used for variables later.
    // It will represent a single voter.
    struct Buyer {
        uint weight; // weight is accumulated by delegation
        bool ticketBought;  // if true, that person already ticketBought
        address delegate; // person delegated to
        uint ticket;   // index of the ticketBought proposal
    }
	
	struct Ticket {
		uint identifier;
		uint price;
		uint number;
		bool sold;
	}

    // This is a type for a single proposal.
    struct Event {
        bytes32 name;   // short name (up to 32 bytes)
        uint numberOfTicketsSold; // number of accumulated votes
		Ticket ticket;
    }

	uint public numTickets;
	Ticket[] public allTickets;
    address public chairperson;

    // This declares a state variable that
    // stores a `Buyer` struct for each possible address.
    mapping(address => Buyer) public buyers;

    // A dynamically-sized array of `Event` structs.
    Event[] public events;

    /// Create a new SellEventTickets to choose one of `proposalNames`.
    function SellEventTickets(bytes32[] eventNames, uint[] _number, uint[] _identifiers, uint[] _prices ) public {
        chairperson = msg.sender;
        buyers[chairperson].weight = 1;
		
        // For each of the provided Event names,
        // create a new Event object and add it
        // to the end of the array.
        for (uint i = 0; i < eventNames.length; i++) {
            // `Event({...})` creates a temporary
            // Event object and `events.push(...)`
            // appends it to the end of `events`.
								
			events.push(Event({
                name: eventNames[i],
                numberOfTicketsSold: 0,
				ticket: Ticket({
						identifier: _identifiers[i],
						price: _prices[i],
						number: _number[i],
						sold: false
						})
            }));
			
			allTickets.push(Ticket({
				identifier: _identifiers[i],
				price: _prices[i],
				number: _number[i],
				sold: false
			}));
        }
		
		numTickets = allTickets.length;
    }
	

    // Give `Buyer` the right to ticket on this SellEventTickets.
    // May only be called by `chairperson`.
    function giveRightToBuy(address buyer) public {
        // If the argument of `require` evaluates to `false`,
        // it terminates and reverts all changes to
        // the state and to Ether balances. It is often
        // a good idea to use this if functions are
        // called incorrectly. But watch out, this
        // will currently also consume all provided gas
        // (this is planned to change in the future).
        require((msg.sender == chairperson) && !buyers[buyer].ticketBought && (buyers[buyer].weight == 0));
        buyers[buyer].weight = 1;
    }

    /// Delegate your ticket to the Buyer `to`.
    function delegate(address to) public {
        // assigns reference
        Buyer storage sender = buyers[msg.sender];
        require(!sender.ticketBought);

        // Self-delegation is not allowed.
        require(to != msg.sender);

        // Forward the delegation as long as
        // `to` also delegated.
        // In general, such loops are very dangerous,
        // because if they run too long, they might
        // need more gas than is available in a block.
        // In this case, the delegation will not be executed,
        // but in other situations, such loops might
        // cause a contract to get "stuck" completely.
        while (buyers[to].delegate != address(0)) {
            to = buyers[to].delegate;

            // We found a loop in the delegation, not allowed.
            require(to != msg.sender);
        }

        // Since `sender` is a reference, this
        // modifies `buyers[msg.sender].ticketBought`
        sender.ticketBought = true;
        sender.delegate = to;
        Buyer storage delegate = buyers[to];
        if (delegate.ticketBought) {
            // If the delegate already ticketBought,
            // directly add to the number of votes
            events[delegate.ticket].numberOfTicketsSold += sender.weight;
        } else {
            // If the delegate did not ticket yet,
            // add to her weight.
            delegate.weight += sender.weight;
        }
    }

    /// Give your ticket (including votes delegated to you)
    /// to Event `events[Event].name`.
    function buyTicket(uint eventName) public {
        Buyer storage sender = buyers[msg.sender];
        require(!sender.ticketBought);
		require(events[eventName].numberOfTicketsSold < events[eventName].ticket.number);
		
        sender.ticketBought = true;
        sender.ticket = eventName;

        // If `Event` is out of the range of the array,
        // this will throw automatically and revert all
        // changes.
        events[eventName].numberOfTicketsSold += sender.weight;
    }

    /// @dev Computes the winning Event taking all
    /// previous votes into account.
    function maxNrOfSoldTickets() public view
            returns (uint winningProposal)
    {
        uint winningVoteCount = 0;
        for (uint p = 0; p < events.length; p++) {
            if (events[p].numberOfTicketsSold > winningVoteCount) {
                winningVoteCount = events[p].numberOfTicketsSold;
                winningProposal = p;
            }
        }
    }
	
	function numberOfTicketsSoldPerEvent(uint eventName) public view
            returns (uint number)
    {
        number = events[eventName].numberOfTicketsSold;
    }

    // Calls winningProposal() function to get the index
    // of the winner contained in the events array and then
    // returns the name of the winner
    function eventWhichSoldMaxTickets() public view
            returns (bytes32 winnerName)
    {
        winnerName = events[maxNrOfSoldTickets()].name;
    }
}