// This is a simple ticket sales contract
//
// TODO: Create tokens, send fees to token holders

pragma solidity ^0.4.8;

contract Blocktix {

    // The organizer of the event
    address public organizer;
    // The eventName of the event
    string public eventName;
    // All tickets
    TicketType[] public tickets;
    // Ticket bids
    TicketBid[] public bids;
	//number of tickets created
	uint public ticketsNumber;
	//number of bided tickets
	uint public bidedTicketsNumber;
	

    struct TicketType {
		uint identifier;
        // A plain text description of the ticket type, i.e.: VIP, Standing, Seated, etc...
        string description;
        // The price of the ticket
        uint price;
        // The amount of tickets available
        uint nrOfTicketsAvailable;
        // The amount of tickets sold
        uint nrOfTicketsSold;
        // map ticket to owner
        mapping (address => uint) paid;
    }

    struct TicketBid {
        // The bidOwner of the bid
        address bidOwner;
        // Is buy or sell bid
        bool buy;
        // Bid price
        uint price;
        // The type of ticket the buyer or seller is bidding
        uint ticketID;
    }

    event BuyTicket(uint _id, address _from, uint _amount); // so you can log the event

    function Blocktix(
        string _name,
		uint _identifier,
        uint _price,
        uint _nrOfTicketsAvailable
    ) {
        organizer = msg.sender;
        eventName = _name;
		ticketsNumber = _nrOfTicketsAvailable;
		tickets.length = ticketsNumber;
		
		for (uint i = 0; i < ticketsNumber; i++) {
			//uint identifier = _identifiers[i];
			tickets[i] = TicketType(_identifier,_name, _price,_nrOfTicketsAvailable,0);
		}
		
		bidedTicketsNumber = 0;

        // Set defaults
		//tickets.push(TicketType(_name, _price,_nrOfTicketsAvailable,0));
       // TicketType t = tickets[tickets.length];
       // t.description = _name;
       // t.price = _price;
        //t.nrOfTicketsAvailable = _nrOfTicketsAvailable;
       // t.nrOfTicketsSold = 0;
      
    }

    function buyTicket(
        uint _ticketID
    ) payable {
        TicketType t = tickets[_ticketID];
        if (t.nrOfTicketsSold >= t.nrOfTicketsAvailable || t.price > msg.value) {
            throw; // throw ensures funds will be returned
        }

        if (t.paid[msg.sender] > 0) {
            throw; // only 1 ticket per address
        }
        t.paid[msg.sender] = msg.value;
        t.nrOfTicketsSold++;
		t.nrOfTicketsAvailable--;
        BuyTicket(_ticketID, msg.sender, msg.value);
    }

    modifier organizerOnly() {
        if (msg.sender != organizer) { throw; }
        _;
    }

    function changeTicketType(
        uint _ticketID,
        string _description,
        uint _nrOfTicketsAvailable
    ) organizerOnly() public {
        TicketType t = tickets[_ticketID];
        if (t.nrOfTicketsSold > _nrOfTicketsAvailable) { return; } // can't make nrOfTicketsAvailable lower than amount already sold
        t.description = _description;
        t.nrOfTicketsAvailable = _nrOfTicketsAvailable;
    }


    function refundTicket(
        address bidOwner,
        uint _ticketID
    ) organizerOnly() public {

        address contractAddress = this;
        if (_ticketID < 0) { // Refund all tickets purchased by bidOwner
            for (uint i=0; i<tickets.length; i++) {
                if (tickets[i].nrOfTicketsSold == 0 || contractAddress.balance < tickets[i].paid[bidOwner] || tickets[i].paid[bidOwner] <= 0) {
                    continue;
                }
                //TODO: Events
                if(!contractAddress.send(tickets[i].paid[bidOwner] - (tickets[i].paid[bidOwner] / 20))) // 5% refund fee
                    throw;
                tickets[i].paid[bidOwner] = 0;
                tickets[i].nrOfTicketsSold--;
            }
        } else {
            TicketType t = tickets[_ticketID];
            if (t.nrOfTicketsSold == 0 || contractAddress.balance < t.paid[bidOwner] || t.paid[bidOwner] <= 0) {
                return;
            }
            //TODO: Events
            if(!bidOwner.send(t.paid[bidOwner] - (t.paid[bidOwner] / 20))) // 5% refund fee
                throw;
            t.paid[bidOwner] = 0;
            t.nrOfTicketsSold--;
        }

        return;
    }

    function transferTicket(
        uint _ticketID,
        address newOwner
    ) public {
        // TODO: Enforce minimum % paid to contract
        TicketType t = tickets[_ticketID];
        if (t.paid[msg.sender] > 0) {
            uint value = t.paid[msg.sender];
            t.paid[msg.sender] = 0;
            t.paid[newOwner] = value;
        }
    }

    function bidTicket(
        uint _ticketID
    ) payable {
        TicketType t = tickets[_ticketID];

        // Make sure the buyer doesn't have a ticket
        if (t.paid[msg.sender] > 0) {
            throw;
        }

		bids.length = bidedTicketsNumber;
		
        // First check bids, buy from seller if there is one
        for (uint i=0; i< bids.length; i++) {
            TicketBid bid = bids[i];
            if (bid.buy == false && bid.ticketID == _ticketID && bid.price == msg.value) {
                // We can buy it from this guy...
                uint ticketValue = t.paid[msg.sender];
                uint saleValue = bid.price;
                t.paid[bid.bidOwner] = 0;
                delete bids[i];
                t.paid[msg.sender] = ticketValue;
                {
                    address contractAddress = this;
                    if (contractAddress.balance >= saleValue) {
                        if(!bid.bidOwner.send(saleValue - (saleValue / 100))) // 1% sale fee
                            throw;
                        return;
                    }
                }
            }
        }

        // Now create the bid...
		bids[bidedTicketsNumber]= TicketBid(msg.sender,true, msg.value,_ticketID);
		bidedTicketsNumber++;
		
        //bid = bids[bids.length];
        //bid.buy = true;
        //bid.price = msg.value;
        //bid.bidOwner = msg.sender;
        //bid.ticketID = _ticketID;
		//bids.push(TicketBid(msg.sender,true, msg.value,_ticketID));
    }

    function sellTicket(
        uint _ticketID
    ) payable {
        TicketType t = tickets[_ticketID];

        // Make sure the seller has a ticket
        if (t.paid[msg.sender] <= 0) {
            throw;
        }

		bids.length = bidedTicketsNumber;
		
        // First check bids, sell to bidder if there is one
        for (uint i=0; i<bids.length; i++) {
            TicketBid bid = bids[i];
            if (bid.buy == true && bid.ticketID == _ticketID && bid.price == msg.value) {
                // We can sell it to this guy...
                uint ticketValue = t.paid[msg.sender];
                uint saleValue = bid.price;
                t.paid[msg.sender] = 0;
                delete bids[i];
                t.paid[bid.bidOwner] = ticketValue;
                {
                    address contractAddress = this;
                    if (contractAddress.balance >= saleValue) {
                        if(!msg.sender.send(saleValue - (saleValue / 100))) // 1% sale fee
                            throw;
                        return;
                    }
                }
            }
        }

		// Now create the bid...
		bids[bidedTicketsNumber]= TicketBid(msg.sender,true, msg.value,_ticketID);
		bidedTicketsNumber++;
		        
        //bid = bids[bids.length];
       // bid.buy = false;
       // bid.price = msg.value;
       // bid.bidOwner = msg.sender;
       // bid.ticketID = _ticketID;
		//bids.push(TicketBid(msg.sender,false, msg.value,_ticketID));
    }

    function cancelBid(
        uint _ticketID,
        uint price,
        bool buy
    ) public {
        for (uint i=0; i<bids.length; i++) {
            TicketBid bid = bids[i];
            if (bid.buy == buy && bid.ticketID == _ticketID && bid.price == price && bid.bidOwner == msg.sender) {
                address contractAddress = this;
                if (contractAddress.balance >= price) {
                    delete bids[i];
                    if(!msg.sender.send(price))
                        throw;
                    return;
                }
            }
        }
    }

    function destroy() {
        if (msg.sender == organizer) {
            suicide(organizer);
        }
    }
}