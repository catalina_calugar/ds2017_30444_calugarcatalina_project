package contracts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.EventValues;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tuples.generated.Tuple2;
import org.web3j.tuples.generated.Tuple4;
import org.web3j.tuples.generated.Tuple7;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.1.1.
 */
public final class Event extends Contract {
    private static final String BINARY = "606060405234156200001057600080fd5b6040516200171b3803806200171b833981016040528080518201919060200180519190602001805182019190602001805182019190602001805182019190602001805160008054600160a060020a03191633600160a060020a031617905591506200009090508686868686866401000000006200009c810262000e3e1704565b505050505050620003d5565b600285905560008080808089620000b560048262000246565b5060018b8051620000cb9291602001906200027a565b50600094505b6002548510156200023957888581518110620000e957fe5b9060200190602002015193508785815181106200010257fe5b9060200190602002015192508685815181106200011b57fe5b90602001906020020151915060e0604051908101604052808c81526020016000600160a060020a03168152602001602060405190810160405280600081525081526020018581526020018481526020018381526020018215158152506004868154811015156200018757fe5b9060005260206000209060070201600082015181908051620001ae9291602001906200027a565b506020820151600182018054600160a060020a031916600160a060020a0392909216919091179055604082015181600201908051620001f29291602001906200027a565b50606082015181600301556080820151816004015560a0820151816005015560c0820151600691909101805460ff19169115159190911790555060019490940193620000d1565b5050505050505050505050565b8154818355818115116200027557600702816007028360005260206000209182019101620002759190620002ff565b505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10620002bd57805160ff1916838001178555620002ed565b82800160010185558215620002ed579182015b82811115620002ed578251825591602001919060010190620002d0565b50620002fb9291506200036d565b5090565b6200036a91905b80821115620002fb5760006200031d82826200038a565b600182018054600160a060020a03191690556200033f6002830160006200038a565b5060006003820181905560048201819055600582015560068101805460ff1916905560070162000306565b90565b6200036a91905b80821115620002fb576000815560010162000374565b50805460018160011615610100020316600290046000825580601f10620003b25750620003d2565b601f016020900490600052602060002090810190620003d291906200036d565b50565b61133680620003e56000396000f3006060604052600436106100c45763ffffffff7c0100000000000000000000000000000000000000000000000000000000600035041663127cae3781146100c957806329e974631461021c578063353d90ec146102415780634ed026221461025457806350b447121461038a5780635586f624146103a057806367dd74ca1461042d5780637dc379fa146104645780638043c9c0146104a65780638cb0ac58146104b95780638da5cb5b146105d9578063b948348c14610608578063e241096414610613575b600080fd5b34156100d457600080fd5b6100e8600160a060020a036004351661063e565b604051600160a060020a0387166020820152606081018590526080810184905260a0810183905281151560c082015260e08082528854600260018216156101009081026000190190921604918301829052829160408301918301908b9080156101925780601f1061016757610100808354040283529160200191610192565b820191906000526020600020905b81548152906001019060200180831161017557829003601f168201915b50508381038252885460026000196101006001841615020190911604808252602090910190899080156102065780601f106101db57610100808354040283529160200191610206565b820191906000526020600020905b8154815290600101906020018083116101e957829003601f168201915b5050995050505050505050505060405180910390f35b341561022757600080fd5b61022f610682565b60405190815260200160405180910390f35b341561024c57600080fd5b61022f610688565b341561025f57600080fd5b61026761068e565b6040518080602001806020018060200180602001858103855289818151815260200191508051906020019060200280838360005b838110156102b357808201518382015260200161029b565b50505050905001858103845288818151815260200191508051906020019060200280838360005b838110156102f25780820151838201526020016102da565b50505050905001858103835287818151815260200191508051906020019060200280838360005b83811015610331578082015183820152602001610319565b50505050905001858103825286818151815260200191508051906020019060200280838360005b83811015610370578082015183820152602001610358565b505050509050019850505050505050505060405180910390f35b341561039557600080fd5b6100e8600435610864565b34156103ab57600080fd5b6103b66004356108b6565b60405160208082528190810183818151815260200191508051906020019080838360005b838110156103f25780820151838201526020016103da565b50505050905090810190601f16801561041f5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561043857600080fd5b61044360043561097d565b604051918252600160a060020a031660208201526040908101905180910390f35b341561046f57600080fd5b61047a600435610d01565b604051938452602084019290925260408084019190915290151560608301526080909101905180910390f35b34156104b157600080fd5b6103b6610da0565b34156104c457600080fd5b6105d760046024813581810190830135806020601f82018190048102016040519081016040528181529291906020840183838082843782019150505050505091908035906020019091908035906020019082018035906020019080806020026020016040519081016040528093929190818152602001838360200280828437820191505050505050919080359060200190820180359060200190808060200260200160405190810160405280939291908181526020018383602002808284378201915050505050509190803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843750949650505050913515159150610e3e9050565b005b34156105e457600080fd5b6105ec610fe7565b604051600160a060020a03909116815260200160405180910390f35b6105d7600435610ff6565b341561061e57600080fd5b61062661114d565b60405191825260208201526040908101905180910390f35b60056020819052600091825260409091206001810154600382015460048301549383015460068401549394600160a060020a03909316936002860193919060ff1687565b60035481565b60025481565b610696611174565b61069e611174565b6106a6611174565b6106ae611174565b6106b6611174565b6106be611174565b6106c6611174565b6106ce611174565b60006002546040518059106106e05750595b908082528060200260200182016040525094506002546040518059106107035750595b908082528060200260200182016040525093506002546040518059106107265750595b908082528060200260200182016040525092506002546040518059106107495750595b90808252806020026020018201604052509150600090505b60025481101561085557600480548290811061077957fe5b90600052602060002090600702016003015485828151811061079757fe5b6020908102909101015260048054829081106107af57fe5b9060005260206000209060070201600401548482815181106107cd57fe5b6020908102909101015260048054829081106107e557fe5b90600052602060002090600702016005015483828151811061080357fe5b60209081029091010152600480548290811061081b57fe5b600091825260209091206006600790920201015460ff1682828151811061083e57fe5b911515602092830290910190910152600101610761565b50929791965094509092509050565b600480548290811061087257fe5b6000918252602090912060079091020160018101546003820154600483015460058401546006850154949550600160a060020a039093169360028601939060ff1687565b6108be611174565b60048054839081106108cc57fe5b90600052602060002090600702016002018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156109715780601f1061094657610100808354040283529160200191610971565b820191906000526020600020905b81548152906001019060200180831161095457829003601f168201915b50505050509050919050565b60008060008060006002546003541115151561099857600080fd5b60048054879081106109a657fe5b906000526020600020906007020160000192506004868154811015156109c857fe5b906000526020600020906007020160020191506004868154811015156109ea57fe5b9060005260206000209060070201600301549450600486815481101515610a0d57fe5b9060005260206000209060070201600401549050600486815481101515610a3057fe5b600091825260209091206005600790920201018054600019019055600480546001919088908110610a5d57fe5b60009182526020909120600790910201600601805460ff191691151591909117905533935060e060405190810160405280848054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610b235780601f10610af857610100808354040283529160200191610b23565b820191906000526020600020905b815481529060010190602001808311610b0657829003601f168201915b5050505050815260200133600160a060020a03168152602001838054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610bd15780601f10610ba657610100808354040283529160200191610bd1565b820191906000526020600020905b815481529060010190602001808311610bb457829003601f168201915b505050918352505060208082018890526040808301859052600160608401819052608090930192909252600160a060020a0387166000908152600590915220815181908051610c24929160200190611186565b50602082015160018201805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055604082015181600201908051610c73929160200190611186565b50606082015181600301556080820151816004015560a0820151816005015560c0820151600691909101805460ff1916911515919091179055506003805460010190557fbfe1212f03228d63c62e49bab81b506b357d17dda584c72f0bad887e271546518633604051918252600160a060020a031660208201526040908101905180910390a1505050915091565b600080600080600485815481101515610d1657fe5b906000526020600020906007020160030154600486815481101515610d3757fe5b906000526020600020906007020160040154600487815481101515610d5857fe5b906000526020600020906007020160050154600488815481101515610d7957fe5b60009182526020909120600790910201600601549298919750955060ff9091169350915050565b60018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610e365780601f10610e0b57610100808354040283529160200191610e36565b820191906000526020600020905b815481529060010190602001808311610e1957829003601f168201915b505050505081565b600285905560008080808089610e55600482611204565b5060018b8051610e69929160200190611186565b50600094505b600254851015610fda57888581518110610e8557fe5b906020019060200201519350878581518110610e9d57fe5b906020019060200201519250868581518110610eb557fe5b90602001906020020151915060e0604051908101604052808c81526020016000600160a060020a0316815260200160206040519081016040528060008152508152602001858152602001848152602001838152602001821515815250600486815481101515610f2057fe5b9060005260206000209060070201600082015181908051610f45929160200190611186565b50602082015160018201805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055604082015181600201908051610f94929160200190611186565b50606082015181600301556080820151816004015560a0820151816005015560c0820151600691909101805460ff19169115159190911790555060019490940193610e6f565b5050505050505050505050565b600054600160a060020a031681565b600480548290811061100457fe5b906000526020600020906007020160040154341015151561102457600080fd5b604051604051908190039020600480548390811061103e57fe5b906000526020600020906007020160020160405180828054600181600116156101000203166002900480156110aa5780601f106110885761010080835404028352918201916110aa565b820191906000526020600020905b815481529060010190602001808311611096575b5050915050604051908190039020146110c257600080fd5b336004828154811015156110d257fe5b906000526020600020906007020160010160006101000a815481600160a060020a030219169083600160a060020a031602179055507fbfe1212f03228d63c62e49bab81b506b357d17dda584c72f0bad887e271546518133604051918252600160a060020a031660208201526040908101905180910390a150565b600160a060020a033316600090815260056020526040902060038101546004909101549091565b60206040519081016040526000815290565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106111c757805160ff19168380011785556111f4565b828001600101855582156111f4579182015b828111156111f45782518255916020019190600101906111d9565b50611200929150611235565b5090565b815481835581811511611230576007028160070283600052602060002091820191016112309190611252565b505050565b61124f91905b80821115611200576000815560010161123b565b90565b61124f91905b8082111561120057600061126c82826112c3565b60018201805473ffffffffffffffffffffffffffffffffffffffff191690556112996002830160006112c3565b5060006003820181905560048201819055600582015560068101805460ff19169055600701611258565b50805460018160011615610100020316600290046000825580601f106112e95750611307565b601f0160209004906000526020600020908101906113079190611235565b505600a165627a7a72305820fbcc81414382fcf5042464c14bbf5d8084dd13ac72984ad730f28c154706636d0029";

    private Event(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    private Event(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public List<PurchaseTicketEventResponse> getPurchaseTicketEvents(TransactionReceipt transactionReceipt) {
        final org.web3j.abi.datatypes.Event event = new org.web3j.abi.datatypes.Event("PurchaseTicket", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Address>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<PurchaseTicketEventResponse> responses = new ArrayList<PurchaseTicketEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            PurchaseTicketEventResponse typedResponse = new PurchaseTicketEventResponse();
            typedResponse._id = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse._from = (String) eventValues.getNonIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<PurchaseTicketEventResponse> purchaseTicketEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final org.web3j.abi.datatypes.Event event = new org.web3j.abi.datatypes.Event("PurchaseTicket", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Address>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, PurchaseTicketEventResponse>() {
            @Override
            public PurchaseTicketEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                PurchaseTicketEventResponse typedResponse = new PurchaseTicketEventResponse();
                typedResponse._id = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse._from = (String) eventValues.getNonIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public RemoteCall<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>> purchasedTickets(String param0) {
        final Function function = new Function("purchasedTickets", 
                Arrays.<Type>asList(new Address(param0)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Address>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Bool>() {}));
        return new RemoteCall<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>>(
                new Callable<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>>() {
                    @Override
                    public Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);;
                        return new Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>(
                                (String) results.get(0).getValue(), 
                                (String) results.get(1).getValue(), 
                                (String) results.get(2).getValue(), 
                                (BigInteger) results.get(3).getValue(), 
                                (BigInteger) results.get(4).getValue(), 
                                (BigInteger) results.get(5).getValue(), 
                                (Boolean) results.get(6).getValue());
                    }
                });
    }

    public RemoteCall<BigInteger> numberOfPurchasedTickets() {
        Function function = new Function("numberOfPurchasedTickets", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<BigInteger> numTickets() {
        Function function = new Function("numTickets", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<Tuple4<List<BigInteger>, List<BigInteger>, List<BigInteger>, List<Boolean>>> getTickets() {
        final Function function = new Function("getTickets", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Uint256>>() {}, new TypeReference<DynamicArray<Uint256>>() {}, new TypeReference<DynamicArray<Uint256>>() {}, new TypeReference<DynamicArray<Bool>>() {}));
        return new RemoteCall<Tuple4<List<BigInteger>, List<BigInteger>, List<BigInteger>, List<Boolean>>>(
                new Callable<Tuple4<List<BigInteger>, List<BigInteger>, List<BigInteger>, List<Boolean>>>() {
                    @Override
                    public Tuple4<List<BigInteger>, List<BigInteger>, List<BigInteger>, List<Boolean>> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);;
                        return new Tuple4<List<BigInteger>, List<BigInteger>, List<BigInteger>, List<Boolean>>(
                                (List<BigInteger>) results.get(0).getValue(), 
                                (List<BigInteger>) results.get(1).getValue(), 
                                (List<BigInteger>) results.get(2).getValue(), 
                                (List<Boolean>) results.get(3).getValue());
                    }
                });
    }

    public RemoteCall<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>> tickets(BigInteger param0) {
        final Function function = new Function("tickets", 
                Arrays.<Type>asList(new Uint256(param0)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Address>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Bool>() {}));
        return new RemoteCall<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>>(
                new Callable<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>>() {
                    @Override
                    public Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);;
                        return new Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>(
                                (String) results.get(0).getValue(), 
                                (String) results.get(1).getValue(), 
                                (String) results.get(2).getValue(), 
                                (BigInteger) results.get(3).getValue(), 
                                (BigInteger) results.get(4).getValue(), 
                                (BigInteger) results.get(5).getValue(), 
                                (Boolean) results.get(6).getValue());
                    }
                });
    }

    public RemoteCall<String> getTicketHash(BigInteger ticketID) {
        Function function = new Function("getTicketHash", 
                Arrays.<Type>asList(new Uint256(ticketID)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> buyTicket(BigInteger ticketID) {
        Function function = new Function(
                "buyTicket", 
                Arrays.<Type>asList(new Uint256(ticketID)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<Tuple4<BigInteger, BigInteger, BigInteger, Boolean>> getTicket(BigInteger ticketID) {
        final Function function = new Function("getTicket", 
                Arrays.<Type>asList(new Uint256(ticketID)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Bool>() {}));
        return new RemoteCall<Tuple4<BigInteger, BigInteger, BigInteger, Boolean>>(
                new Callable<Tuple4<BigInteger, BigInteger, BigInteger, Boolean>>() {
                    @Override
                    public Tuple4<BigInteger, BigInteger, BigInteger, Boolean> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);;
                        return new Tuple4<BigInteger, BigInteger, BigInteger, Boolean>(
                                (BigInteger) results.get(0).getValue(), 
                                (BigInteger) results.get(1).getValue(), 
                                (BigInteger) results.get(2).getValue(), 
                                (Boolean) results.get(3).getValue());
                    }
                });
    }

    public RemoteCall<String> eventName() {
        Function function = new Function("eventName", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> createTickets(String _eventName, BigInteger _numTickets, List<BigInteger> _identifiers, List<BigInteger> _prices, List<BigInteger> _number, Boolean _isSold) {
        Function function = new Function(
                "createTickets", 
                Arrays.<Type>asList(new Utf8String(_eventName),
                new Uint256(_numTickets),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_identifiers, Uint256.class)),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_prices, Uint256.class)),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_number, Uint256.class)),
                new Bool(_isSold)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        Function function = new Function("owner", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> purchaseTicket(BigInteger ticketID, BigInteger weiValue) {
        Function function = new Function(
                "purchaseTicket", 
                Arrays.<Type>asList(new Uint256(ticketID)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public RemoteCall<Tuple2<BigInteger, BigInteger>> getPurchasedTicket() {
        final Function function = new Function("getPurchasedTicket", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
        return new RemoteCall<Tuple2<BigInteger, BigInteger>>(
                new Callable<Tuple2<BigInteger, BigInteger>>() {
                    @Override
                    public Tuple2<BigInteger, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);;
                        return new Tuple2<BigInteger, BigInteger>(
                                (BigInteger) results.get(0).getValue(), 
                                (BigInteger) results.get(1).getValue());
                    }
                });
    }

    public static RemoteCall<Event> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String _eventName, BigInteger _numTickets, List<BigInteger> _identifiers, List<BigInteger> _prices, List<BigInteger> _number, Boolean _isSold) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_eventName),
                new Uint256(_numTickets),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_identifiers, Uint256.class)),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_prices, Uint256.class)),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_number, Uint256.class)),
                new Bool(_isSold)));
        return deployRemoteCall(Event.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static RemoteCall<Event> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String _eventName, BigInteger _numTickets, List<BigInteger> _identifiers, List<BigInteger> _prices, List<BigInteger> _number, Boolean _isSold) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_eventName),
                new Uint256(_numTickets),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_identifiers, Uint256.class)),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_prices, Uint256.class)),
                new DynamicArray<Uint256>(
                        org.web3j.abi.Utils.typeMap(_number, Uint256.class)),
                new Bool(_isSold)));
        return deployRemoteCall(Event.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static Event load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Event(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static Event load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Event(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static class PurchaseTicketEventResponse {
        public BigInteger _id;

        public String _from;
    }
}
