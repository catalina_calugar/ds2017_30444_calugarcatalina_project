package contracts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.EventValues;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tuples.generated.Tuple4;
import org.web3j.tuples.generated.Tuple5;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.1.1.
 */
public final class Blocktix extends Contract {
    private static final String BINARY = "606060405234156200001057600080fd5b6040516200132e3803806200132e83398101604052808051820191906020018051919060200180519190602001805160008054600160a060020a03191633600160a060020a031617815590925090506001858051620000749291602001906200013e565b5060048290558162000088600282620001c3565b50600090505b6004548110156200012d5760a0604051908101604052808581526020018681526020018481526020018381526020016000815250600282815481101515620000d257fe5b906000526020600020906006020160008201518155602082015181600101908051620001039291602001906200013e565b5060408201518160020155606082015181600301556080820151600490910155506001016200008e565b5050600060055550620002a5915050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106200018157805160ff1916838001178555620001b1565b82800160010185558215620001b1579182015b82811115620001b157825182559160200191906001019062000194565b50620001bf929150620001f7565b5090565b815481835581811511620001f257600602816006028360005260206000209182019101620001f2919062000217565b505050565b6200021491905b80821115620001bf5760008155600101620001fe565b90565b6200021491905b80821115620001bf5760008082556200023b60018301826200025a565b506000600282018190556003820181905560048201556006016200021e565b50805460018160011615610100020316600290046000825580601f10620002825750620002a2565b601f016020900490600052602060002090810190620002a29190620001f7565b50565b61107980620002b56000396000f3006060604052600436106100cf5763ffffffff7c0100000000000000000000000000000000000000000000000000000000600035041663115b872681146100d45780634423c5f1146100f957806350b44712146101455780636120326514610202578063679ebf1a1461023157806367dd74ca14610255578063705099b9146102605780638043c9c01461028257806383197ef01461030c578063bbaa5cd11461031f578063caa7fd0614610332578063e5ea120114610350578063e87c1463146103a8578063f192866a146103b3575b600080fd5b34156100df57600080fd5b6100e76103be565b60405190815260200160405180910390f35b341561010457600080fd5b61010f6004356103c4565b604051600160a060020a039094168452911515602084015260408084019190915260608301919091526080909101905180910390f35b341561015057600080fd5b61015b60043561040b565b60405185815260408101849052606081018390526080810182905260a0602082018181528654600260001961010060018416150201909116049183018290529060c0830190879080156101ef5780601f106101c4576101008083540402835291602001916101ef565b820191906000526020600020905b8154815290600101906020018083116101d257829003601f168201915b5050965050505050505060405180910390f35b341561020d57600080fd5b610215610448565b604051600160a060020a03909116815260200160405180910390f35b341561023c57600080fd5b610253600435600160a060020a0360243516610457565b005b6102536004356104ce565b341561026b57600080fd5b610253600160a060020a03600435166024356105c6565b341561028d57600080fd5b6102956108e9565b60405160208082528190810183818151815260200191508051906020019080838360005b838110156102d15780820151838201526020016102b9565b50505050905090810190601f1680156102fe5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561031757600080fd5b610253610987565b341561032a57600080fd5b6100e76109ae565b341561033d57600080fd5b61025360043560243560443515156109b4565b341561035b57600080fd5b610253600480359060446024803590810190830135806020601f820181900481020160405190810160405281815292919060208401838380828437509496505093359350610ad492505050565b610253600435610b3d565b610253600435610dbd565b60045481565b60038054829081106103d257fe5b6000918252602090912060039091020180546001820154600290920154600160a060020a038216935060a060020a90910460ff16919084565b600280548290811061041957fe5b600091825260209091206006909102018054600282015460038301546004840154929450600190930192909185565b600054600160a060020a031681565b60008060028481548110151561046957fe5b60009182526020808320600160a060020a033316845260056006909302019182019052604082205490935011156104c85750600160a060020a033381166000908152600583016020526040808220805490839055928516825290208190555b50505050565b60006002828154811015156104df57fe5b90600052602060002090600602019050806003015481600401541015806105095750348160020154115b1561051357600080fd5b600160a060020a0333166000908152600582016020526040812054111561053957600080fd5b33600160a060020a038116600090815260058301602052604090819020349081905560048401805460010190556003840180546000190190557f9df6c7b3dde09a81ddfae6fb8f165a5d4e62287c79a1faf24bc247908620436192859290919051928352600160a060020a0390911660208301526040808301919091526060909101905180910390a15050565b600080548190819033600160a060020a039081169116146105e657600080fd5b30925060008410156107f657600091505b6002548210156107f157600280548390811061060f57fe5b9060005260206000209060060201600401546000148061067a5750600280548390811061063857fe5b9060005260206000209060060201600501600086600160a060020a0316600160a060020a031681526020019081526020016000205483600160a060020a031631105b806106bd5750600060028381548110151561069157fe5b60009182526020808320600160a060020a038a1684526005600690930201919091019052604090205411155b156106c7576107e6565b82600160a060020a03166108fc60146002858154811015156106e557fe5b60009182526020808320600160a060020a038c1684526005600690930201919091019052604090205481151561071757fe5b0460028581548110151561072757fe5b9060005260206000209060060201600501600089600160a060020a0316600160a060020a0316815260200190815260200160002054039081150290604051600060405180830381858888f19350505050151561078257600080fd5b600060028381548110151561079357fe5b60009182526020808320600160a060020a038a1684526005600690930201919091019052604090205560028054839081106107ca57fe5b6000918252602090912060046006909202010180546000190190555b6001909101906105f7565b6108e2565b600280548590811061080457fe5b906000526020600020906006020190508060040154600014806108445750600160a060020a03808616600090815260058301602052604090205490841631105b806108685750600160a060020a038516600090815260058201602052604081205411155b15610872576108e2565b600160a060020a0385166000818152600583016020526040908190205460148104900380156108fc029151600060405180830381858888f1935050505015156108ba57600080fd5b600160a060020a03851660009081526005820160205260408120556004810180546000190190555b5050505050565b60018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561097f5780601f106109545761010080835404028352916020019161097f565b820191906000526020600020905b81548152906001019060200180831161096257829003601f168201915b505050505081565b60005433600160a060020a03908116911614156109ac57600054600160a060020a0316ff5b565b60055481565b600080805b600354831015610acc5760038054849081106109d157fe5b60009182526020909120600390910201805490925060ff60a060020a909104161515841515148015610a065750858260020154145b8015610a155750848260010154145b8015610a2e5750815433600160a060020a039081169116145b15610ac1575030600160a060020a03811631859010610ac1576003805484908110610a5557fe5b600091825260208220600390910201805474ffffffffffffffffffffffffffffffffffffffffff19168155600181018290556002015533600160a060020a031685156108fc0286604051600060405180830381858888f193505050501515610abc57600080fd5b610acc565b6001909201916109b9565b505050505050565b6000805433600160a060020a03908116911614610af057600080fd5b6002805485908110610afe57fe5b906000526020600020906006020190508181600401541115610b1f576104c8565b60018101838051610b34929160200190610f41565b50600301555050565b600080600080600080600287815481101515610b5557fe5b60009182526020808320600160a060020a03331684526005600690930201918201905260408220549097501115610b8b57600080fd5b600554610b99600382610fbf565b50600094505b600354851015610ced576003805486908110610bb757fe5b60009182526020909120600390910201805490945060a060020a900460ff16158015610be65750868460020154145b8015610bf55750348460010154145b15610ce257600160a060020a03338116600090815260058801602052604080822054600188015488549094168352908220919091556003805491955091935086908110610c3e57fe5b600091825260208083206003909202909101805474ffffffffffffffffffffffffffffffffffffffffff191681556001810183905560020182905533600160a060020a039081168352600589019091526040909120849055309150811631829010610ce2578354600160a060020a03166108fc6064845b0484039081150290604051600060405180830381858888f193505050501515610cdd57600080fd5b610db4565b600190940193610b9f565b60806040519081016040528033600160a060020a03168152602001600115158152602001348152602001888152506003600554815481101515610d2c57fe5b90600052602060002090600302016000820151815473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03919091161781556020820151815490151560a060020a0274ff000000000000000000000000000000000000000019909116178155604082015181600101556060820151600290910155506005805460010190555b50505050505050565b600080600080600080600287815481101515610dd557fe5b60009182526020808320600160a060020a033316845260056006909302019182019052604082205490975011610e0a57600080fd5b600554610e18600382610fbf565b50600094505b600354851015610ced576003805486908110610e3657fe5b906000526020600020906003020193508360000160149054906101000a900460ff16151560011515148015610e6e5750868460020154145b8015610e7d5750348460010154145b15610f3657600160a060020a0333166000908152600587016020526040812080546001870154929091556003805491955091935086908110610ebb57fe5b600091825260208083206003909202909101805474ffffffffffffffffffffffffffffffffffffffffff19168155600181018390556002018290558554600160a060020a039081168352600589019091526040909120849055309150811631829010610f3657600160a060020a0333166108fc606484610cb5565b600190940193610e1e565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10610f8257805160ff1916838001178555610faf565b82800160010185558215610faf579182015b82811115610faf578251825591602001919060010190610f94565b50610fbb929150610ff0565b5090565b815481835581811511610feb57600302816003028360005260206000209182019101610feb919061100d565b505050565b61100a91905b80821115610fbb5760008155600101610ff6565b90565b61100a91905b80821115610fbb57805474ffffffffffffffffffffffffffffffffffffffffff1916815560006001820181905560028201556003016110135600a165627a7a72305820930140669b6c5bafb41e9e2ad5bcd52bf6ea17ddcbd965483b728bf43787be390029";

    private Blocktix(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    private Blocktix(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public List<BuyTicketEventResponse> getBuyTicketEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("BuyTicket",
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Address>() {}, new TypeReference<Uint256>() {}));
        List<EventValues> valueList = extractEventParameters(event, transactionReceipt);
        ArrayList<BuyTicketEventResponse> responses = new ArrayList<BuyTicketEventResponse>(valueList.size());
        for (EventValues eventValues : valueList) {
            BuyTicketEventResponse typedResponse = new BuyTicketEventResponse();
            typedResponse._id = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse._from = (String) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse._amount = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<BuyTicketEventResponse> buyTicketEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("BuyTicket",
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Address>() {}, new TypeReference<Uint256>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, BuyTicketEventResponse>() {
            @Override
            public BuyTicketEventResponse call(Log log) {
                EventValues eventValues = extractEventParameters(event, log);
                BuyTicketEventResponse typedResponse = new BuyTicketEventResponse();
                typedResponse._id = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse._from = (String) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse._amount = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public RemoteCall<BigInteger> ticketsNumber() {
        Function function = new Function("ticketsNumber",
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<Tuple4<String, Boolean, BigInteger, BigInteger>> bids(BigInteger param0) {
        final Function function = new Function("bids",
                Arrays.<Type>asList(new Uint256(param0)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Bool>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
        return new RemoteCall<Tuple4<String, Boolean, BigInteger, BigInteger>>(
                new Callable<Tuple4<String, Boolean, BigInteger, BigInteger>>() {
                    @Override
                    public Tuple4<String, Boolean, BigInteger, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);;
                        return new Tuple4<String, Boolean, BigInteger, BigInteger>(
                                (String) results.get(0).getValue(),
                                (Boolean) results.get(1).getValue(),
                                (BigInteger) results.get(2).getValue(),
                                (BigInteger) results.get(3).getValue());
                    }
                });
    }

    public RemoteCall<Tuple5<BigInteger, String, BigInteger, BigInteger, BigInteger>> tickets(BigInteger param0) {
        final Function function = new Function("tickets",
                Arrays.<Type>asList(new Uint256(param0)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
        return new RemoteCall<Tuple5<BigInteger, String, BigInteger, BigInteger, BigInteger>>(
                new Callable<Tuple5<BigInteger, String, BigInteger, BigInteger, BigInteger>>() {
                    @Override
                    public Tuple5<BigInteger, String, BigInteger, BigInteger, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);;
                        return new Tuple5<BigInteger, String, BigInteger, BigInteger, BigInteger>(
                                (BigInteger) results.get(0).getValue(),
                                (String) results.get(1).getValue(),
                                (BigInteger) results.get(2).getValue(),
                                (BigInteger) results.get(3).getValue(),
                                (BigInteger) results.get(4).getValue());
                    }
                });
    }

    public RemoteCall<String> organizer() {
        Function function = new Function("organizer",
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> transferTicket(BigInteger _ticketID, String newOwner) {
        Function function = new Function(
                "transferTicket",
                Arrays.<Type>asList(new Uint256(_ticketID),
                new Address(newOwner)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> buyTicket(BigInteger _ticketID, BigInteger weiValue) {
        Function function = new Function(
                "buyTicket",
                Arrays.<Type>asList(new Uint256(_ticketID)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public RemoteCall<TransactionReceipt> refundTicket(String bidOwner, BigInteger _ticketID) {
        Function function = new Function(
                "refundTicket",
                Arrays.<Type>asList(new Address(bidOwner),
                new Uint256(_ticketID)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> eventName() {
        Function function = new Function("eventName",
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> destroy() {
        Function function = new Function(
                "destroy",
                Arrays.<Type>asList(),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<BigInteger> bidedTicketsNumber() {
        Function function = new Function("bidedTicketsNumber",
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> cancelBid(BigInteger _ticketID, BigInteger price, Boolean buy) {
        Function function = new Function(
                "cancelBid",
                Arrays.<Type>asList(new Uint256(_ticketID),
                new Uint256(price),
                new Bool(buy)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> changeTicketType(BigInteger _ticketID, String _description, BigInteger _nrOfTicketsAvailable) {
        Function function = new Function(
                "changeTicketType",
                Arrays.<Type>asList(new Uint256(_ticketID),
                new Utf8String(_description),
                new Uint256(_nrOfTicketsAvailable)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> bidTicket(BigInteger _ticketID, BigInteger weiValue) {
        Function function = new Function(
                "bidTicket",
                Arrays.<Type>asList(new Uint256(_ticketID)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public RemoteCall<TransactionReceipt> sellTicket(BigInteger _ticketID, BigInteger weiValue) {
        Function function = new Function(
                "sellTicket",
                Arrays.<Type>asList(new Uint256(_ticketID)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public static RemoteCall<Blocktix> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String _name, BigInteger _identifier, BigInteger _price, BigInteger _nrOfTicketsAvailable) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_name),
                new Uint256(_identifier),
                new Uint256(_price),
                new Uint256(_nrOfTicketsAvailable)));
        return deployRemoteCall(Blocktix.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static RemoteCall<Blocktix> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String _name, BigInteger _identifier, BigInteger _price, BigInteger _nrOfTicketsAvailable) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_name),
                new Uint256(_identifier),
                new Uint256(_price),
                new Uint256(_nrOfTicketsAvailable)));
        return deployRemoteCall(Blocktix.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static Blocktix load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Blocktix(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static Blocktix load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Blocktix(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static class BuyTicketEventResponse {
        public BigInteger _id;

        public String _from;

        public BigInteger _amount;
    }
}
