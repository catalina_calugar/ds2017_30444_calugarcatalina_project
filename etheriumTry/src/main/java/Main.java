import contracts.Event;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tuples.generated.Tuple5;
import org.web3j.tuples.generated.Tuple7;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import ui.EventUIBuyer;
import ui.EventUISeller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cata_ on 11/29/2017.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Web3j web3j = Web3j.build(new HttpService("http://localhost:8101"));    //rpcport set on miner
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().sendAsync().get();
        String clientVersion = web3ClientVersion.getWeb3ClientVersion();
        System.out.println(clientVersion);

        EthGetBalance ethGetBalance = web3j.ethGetBalance("0x5af9fb9f38551a169602790f01da695d062a4125", DefaultBlockParameter.valueOf("latest")).send();
        System.out.println("========= Balance======== ");
        System.out.println("id: " + ethGetBalance.getId());
        System.out.println("error: " + ethGetBalance.getError());
        System.out.println("result: " + ethGetBalance.getBalance());

        Credentials credentials = WalletUtils.loadCredentials("catalina10", "C:\\etherium\\node1 -dataDir\\keystore\\UTC--2017-11-24T14-40-33.935325000Z--5af9fb9f38551a169602790f01da695d062a4125");
//        Credentials credentialsN2 = WalletUtils.loadCredentials("pass2", "C:\\etherium\\node2 -dataDir\\keystore\\UTC--2018-01-14T16-50-27.345987400Z--c63eeede6f1a434f88e7d8b4f6daae3ee2c3be30");

        List<BigInteger> _identifiers = new ArrayList<>();
        _identifiers.add(BigInteger.valueOf(10));
        _identifiers.add(BigInteger.valueOf(11));
        _identifiers.add(BigInteger.valueOf(12));
        _identifiers.add(BigInteger.valueOf(13));
        _identifiers.add(BigInteger.valueOf(14));

        List<BigInteger> _prices = new ArrayList<>();
        _prices.add(BigInteger.valueOf(20));
        _prices.add(BigInteger.valueOf(30));
        _prices.add(BigInteger.valueOf(40));
        _prices.add(BigInteger.valueOf(50));
        _prices.add(BigInteger.valueOf(60));

        List<BigInteger> _numbers = new ArrayList<>();
        _numbers.add(BigInteger.valueOf(100));
        _numbers.add(BigInteger.valueOf(100));
        _numbers.add(BigInteger.valueOf(100));
        _numbers.add(BigInteger.valueOf(100));
        _numbers.add(BigInteger.valueOf(100));

        System.out.println("========= contracts: Event Contract ========");
//        RemoteCall<Event> eventRemoteCall = Event.deploy(web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT,
//                "Event 1", BigInteger.valueOf(_identifiers.size()), _identifiers, _prices, _numbers, false);
//        Event event = eventRemoteCall.send();

        Event event = Event.load("0x4df92c8e4b3832330168d48b47fb35c6ea6b2976", web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);

        System.out.println("Address: " + event.getContractAddress());
        System.out.println("Num tickets " + event.numTickets().send());

        // RemoteCall<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>> tickets = event.tickets(BigInteger.valueOf(0));
        // System.out.println(" " + tickets.send().getValue1() + " " + tickets.send().getValue2() + " " + tickets.send().getValue3() +
        //        " " + tickets.send().getValue4() + " " + tickets.send().getValue5() + " " + tickets.send().getValue6() + " " + tickets.send().getValue7());

        //RemoteCall<Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean>> purchasedTickets = event.purchasedTickets("0x5af9fb9f38551a169602790f01da695d062a4125");
        //Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean> pt = purchasedTickets.send();

        System.out.println("Purchased ticket id: " + event.buyTicket(BigInteger.valueOf(1)).send());
        System.out.print("After buy" + event.getPurchaseTicketEvents(event.getTransactionReceipt().get()));

        //System.out.println(" " + pt.getValue1() + " " + purchasedTickets.send().getValue2() + " " + purchasedTickets.send().getValue3() + " " + purchasedTickets.send().getValue4());
        //System.out.println("Purchased tickets nr: " + event.numberOfPurchasedTickets().send());
        //System.out.println("Purchased tickets event: " + event.getPurchaseTicketEvents(event.getTransactionReceipt().get()));
        System.out.println();

    }
}
