package ui;

import contracts.Event;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tuples.generated.Tuple2;
import org.web3j.tuples.generated.Tuple7;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class EventUIBuyer {

    private JFrame frmUserFrame;
    private JTable buyerTabel;
    private JTextField eventAddrTextField;
    Event event;
    private JTextField quantityTextField;
    JLabel lblBoughtTicketId;

    public EventUIBuyer() {
        initialize();
        frmUserFrame.setVisible(true);
    }

    private void initialize() {
        frmUserFrame = new JFrame();
        frmUserFrame.setTitle("User Frame");
        frmUserFrame.setBounds(100, 100, 735, 507);
        frmUserFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmUserFrame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBackground(SystemColor.info);
        panel.setBounds(0, 0, 717, 460);
        frmUserFrame.getContentPane().add(panel);
        panel.setLayout(null);

        JButton btnBuyTicket = new JButton("Buy ticket");
        btnBuyTicket.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //buy ticket
                try {
                    buyTicket();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnBuyTicket.setBounds(375, 365, 299, 40);
        panel.add(btnBuyTicket);

        lblBoughtTicketId = new JLabel(" ");
        lblBoughtTicketId.setBounds(149, 418, 453, 16);
        panel.add(lblBoughtTicketId);

        String[] columns = {"ID", "Price", "NumberAvailable"};
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);

        buyerTabel = new JTable(tableModel);
        buyerTabel.setBounds(31, 135, 643, 191);
        panel.add(buyerTabel);

        eventAddrTextField = new JTextField();
        eventAddrTextField.setBounds(105, 60, 460, 22);
        panel.add(eventAddrTextField);
        eventAddrTextField.setColumns(10);

        JLabel lblLoadEvent = new JLabel("Load event:");
        lblLoadEvent.setBounds(31, 63, 107, 16);
        panel.add(lblLoadEvent);

        JButton btnLoad = new JButton("LOAD");
        btnLoad.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                //load event
                try {
                    loadEvent(tableModel);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (CipherException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnLoad.setBounds(577, 59, 97, 25);
        panel.add(btnLoad);

        JLabel lbSuccessLoad = new JLabel("");
        lbSuccessLoad.setForeground(Color.RED);
        lbSuccessLoad.setBounds(31, 95, 643, 16);
        panel.add(lbSuccessLoad);

        quantityTextField = new JTextField();
        quantityTextField.setBounds(105, 374, 258, 22);
        panel.add(quantityTextField);
        quantityTextField.setColumns(10);

        JLabel lblQuantity = new JLabel("Quantity:");
        lblQuantity.setBounds(31, 377, 56, 16);
        panel.add(lblQuantity);
    }

    public void loadEvent(DefaultTableModel tableModel) throws Exception {
        int rowCount = tableModel.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }

        String contractAddress = EventUISeller.contractAddress;
        if (contractAddress == null || contractAddress.equals("")) {
            contractAddress = eventAddrTextField.getText();
        } else {
            eventAddrTextField.setText(EventUISeller.contractAddress);
        }

        Web3j web3j = Web3j.build(new HttpService("http://localhost:8101"));    //rpcport set on miner
        Credentials credentials = WalletUtils.loadCredentials("catalina10", "C:\\etherium\\node1 -dataDir\\keystore\\UTC--2017-11-24T14-40-33.935325000Z--5af9fb9f38551a169602790f01da695d062a4125");

        event = Event.load(contractAddress, web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
        BigInteger nrOfTickets = event.numTickets().send();

        for (int i = 0; i < nrOfTickets.intValue(); i++) {
            Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean> tickets = event.tickets(BigInteger.valueOf(i)).send();
            Object[] row = {tickets.getValue4().toString(), tickets.getValue5() + " ETH", tickets.getValue6() + " available"};
            tableModel.addRow(row);
        }
    }

    public void buyTicket() throws Exception {

        int selection = buyerTabel.getSelectedRow();
        int selectedTicketID = Integer.parseInt(buyerTabel.getValueAt(selection, 0).toString());
        int quantity = Integer.parseInt(quantityTextField.getText());
//        Tuple2<BigInteger, String> buyTicket = event.buyTicket(BigInteger.valueOf(selection)).send();
//        lblBoughtTicketId.setText("Ticket purchased: ID:" + buyTicket.getValue1() + " new owner " + buyTicket.getValue2());

    }

    public static void main(String[] args) throws Exception {
        new EventUIBuyer();
    }

}
