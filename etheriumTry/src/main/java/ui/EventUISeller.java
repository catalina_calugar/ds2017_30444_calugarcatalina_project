package ui;

import contracts.Event;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tuples.generated.Tuple7;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class EventUISeller {

    private JFrame frame;
    private JTextField getTicketByIdTextField;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    JTextArea resultsTextArea;
    JLabel lblResults;

    Event event;
    public static String contractAddress = null;

    public EventUISeller() {
        initialize();
        frame.setVisible(true);
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 803, 494);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(255, 250, 240));
        panel.setBounds(0, 0, 785, 447);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        getTicketByIdTextField = new JTextField();
        getTicketByIdTextField.setBounds(36, 308, 252, 22);
        panel.add(getTicketByIdTextField);
        getTicketByIdTextField.setColumns(10);

        JLabel lblGetTicketBy = new JLabel("Get ticket by ID:");
        lblGetTicketBy.setBounds(36, 272, 138, 22);
        panel.add(lblGetTicketBy);

        JButton btnSearchTicketByID = new JButton("Search");
        btnSearchTicketByID.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //Search ticket by id
                int id = Integer.parseInt(getTicketByIdTextField.getText());
                try {
                    searchTicketByID(id);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnSearchTicketByID.setBounds(317, 307, 97, 25);
        panel.add(btnSearchTicketByID);

        resultsTextArea = new JTextArea();
        resultsTextArea.setBounds(466, 60, 289, 325);
        panel.add(resultsTextArea);

        lblResults = new JLabel("Results:");
        lblResults.setBounds(466, 40, 56, 16);
        panel.add(lblResults);

        textField = new JTextField();
        textField.setBounds(118, 40, 302, 22);
        panel.add(textField);
        textField.setColumns(10);

        JLabel lblType = new JLabel("Event name:");
        lblType.setBounds(30, 43, 76, 16);
        panel.add(lblType);

        JLabel lblDescription = new JLabel("Number of tickets:");
        lblDescription.setBounds(30, 78, 105, 16);
        panel.add(lblDescription);

        textField_1 = new JTextField();
        textField_1.setBounds(147, 75, 273, 22);
        panel.add(textField_1);
        textField_1.setColumns(10);

        JButton btnCreateEvent = new JButton("CREATE EVENT AND TICKETS");
        btnCreateEvent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //Create event + deploy
                try {
                    createEvent(textField.getText(), Integer.parseInt(textField_1.getText()),
                            Integer.parseInt(textField_2.getText()), Integer.parseInt(textField_3.getText()));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnCreateEvent.setBounds(30, 188, 390, 33);
        panel.add(btnCreateEvent);

        textField_2 = new JTextField();
        textField_2.setBounds(118, 110, 302, 22);
        panel.add(textField_2);
        textField_2.setColumns(10);

        JLabel lblFirstTicketId = new JLabel("First ticket ID:");
        lblFirstTicketId.setBounds(30, 113, 105, 16);
        panel.add(lblFirstTicketId);

        JLabel lblTicketPrice = new JLabel("Ticket price:");
        lblTicketPrice.setBounds(30, 148, 76, 16);
        panel.add(lblTicketPrice);

        textField_3 = new JTextField();
        textField_3.setBounds(118, 145, 302, 22);
        panel.add(textField_3);
        textField_3.setColumns(10);

        JButton btnGetAllTickets = new JButton("Get all tickets");
        btnGetAllTickets.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //get all tickets
                try {
                    getAllTickets();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnGetAllTickets.setBounds(36, 343, 384, 25);
        panel.add(btnGetAllTickets);
    }

    public void createEvent(String evName, int numberOfTickets, int fitstTicketId, int price) throws Exception {
        Web3j web3j = Web3j.build(new HttpService("http://localhost:8101"));    //rpcport set on miner
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().sendAsync().get();
        String clientVersion = web3ClientVersion.getWeb3ClientVersion();
        System.out.println(clientVersion);

        EthGetBalance ethGetBalance = web3j.ethGetBalance("0x5af9fb9f38551a169602790f01da695d062a4125", DefaultBlockParameter.valueOf("latest")).send();
        System.out.println("========= Balance======== ");
        System.out.println("id: " + ethGetBalance.getId());
        System.out.println("error: " + ethGetBalance.getError());
        System.out.println("result: " + ethGetBalance.getBalance());
        Credentials credentials = WalletUtils.loadCredentials("catalina10", "C:\\etherium\\node1 -dataDir\\keystore\\UTC--2017-11-24T14-40-33.935325000Z--5af9fb9f38551a169602790f01da695d062a4125");

        List<BigInteger> _identifiers = new ArrayList<>();
        List<BigInteger> _prices = new ArrayList<>();
        List<BigInteger> _numbers = new ArrayList<>();
        _identifiers.add(BigInteger.valueOf(fitstTicketId));
        _prices.add(BigInteger.valueOf(Long.parseLong(String.valueOf(price))));
        _numbers.add(BigInteger.valueOf(numberOfTickets));

        for (int i = 1; i < numberOfTickets; i++) {
            _identifiers.add(BigInteger.valueOf(fitstTicketId + i));
            _prices.add(BigInteger.valueOf(Long.parseLong(String.valueOf(price))));
            _numbers.add(BigInteger.valueOf(numberOfTickets));
        }

        RemoteCall<Event> eventRemoteCall = Event.deploy(web3j, credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT,
                evName, BigInteger.valueOf(numberOfTickets), _identifiers, _prices, _numbers, false);
        event = eventRemoteCall.send();
        contractAddress = event.getContractAddress();
        System.out.println("Address: " + event.getContractAddress());

        resultsTextArea.setText("Contract deployed sucessfuly!\n Address: " + contractAddress);
    }

    public void searchTicketByID(int id) throws Exception {
        Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean> tickets = event.tickets(BigInteger.valueOf(id)).send();
        resultsTextArea.setText("");
        resultsTextArea.setText(resultsTextArea.getText()+"Ticket " + id+ "\n");
        resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue1()+"\n");
        resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue2()+"\n");
        resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue3()+"\n");
        resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue4()+"\n");
        resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue5()+"\n");
        resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue6()+"\n");
        resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue7()+"\n");
    }

    public void getAllTickets() throws Exception {
        BigInteger nrOfTickets = event.numTickets().send();

        resultsTextArea.setText("");
        for (int b = 0; b < nrOfTickets.intValue(); b++) {
            Tuple7<String, String, String, BigInteger, BigInteger, BigInteger, Boolean> tickets = event.tickets(BigInteger.valueOf(b)).send();
            resultsTextArea.setText(resultsTextArea.getText()+"Ticket " + b+ "\n");
            resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue1()+"\n");
            resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue2()+"\n");
            resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue3()+"\n");
            resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue4()+"\n");
            resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue5()+"\n");
            resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue6()+"\n");
            resultsTextArea.setText(resultsTextArea.getText()+tickets.getValue7()+"\n");
        }
    }

    public static void main(String[] args) throws Exception {
        new EventUISeller();
    }
}
