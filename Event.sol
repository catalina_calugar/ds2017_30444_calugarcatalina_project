pragma solidity^0.4.4;

        contract Event{
        address public owner;

        struct Ticket{
			string eventName;
			address owner;
			string ownerHash;
			uint identifier;
			uint256 price;
			uint number;
			bool isSold;
        }

        string public eventName;
        uint public numTickets;
        uint public numberOfPurchasedTickets;
        Ticket[]public tickets;
        //Ticket[] public purchasedTickets;
        mapping(address => Ticket) public purchasedTickets;

        event PurchaseTicket(uint _id,address _from); // so you can log the event

        function Event(string _eventName,uint _numTickets,uint[]_identifiers,uint[]_prices,uint[]_number,bool _isSold){
			owner=msg.sender;
			createTickets(_eventName,_numTickets,_identifiers,_prices, _number, _isSold);
        }

        function createTickets(string _eventName,uint _numTickets,uint[]_identifiers,uint[]_prices,uint[]_number,bool _isSold) public {
			numTickets=_numTickets;
			tickets.length=numTickets;
			eventName=_eventName;

			for(uint i=0;i<numTickets; i++){
				uint identifier=_identifiers[i];
				uint256 price=_prices[i];
				uint number=_number[i];
				bool isSold=isSold;
				tickets[i]=Ticket(_eventName,address(0),"",identifier,price,number,isSold);
			}
        }

        function buyTicket(uint ticketID) public returns(uint identifier,address newOwner){
			require(numberOfPurchasedTickets <= numTickets);

			string evName = tickets[ticketID].eventName;
			string ownerHash = tickets[ticketID].ownerHash;
			identifier = tickets[ticketID].identifier;
			uint256 price = tickets[ticketID].price;
			tickets[ticketID].number--;
			tickets[ticketID].isSold = true;
			
			newOwner = msg.sender;

			purchasedTickets[newOwner] = Ticket(evName,msg.sender,ownerHash,identifier, price, 1,true);
			numberOfPurchasedTickets++;
			
			//purchaseTicket(ticketID);

			PurchaseTicket(ticketID,msg.sender);

        }

        function purchaseTicket(uint ticketID) payable {
			require(msg.value >= tickets[ticketID].price);
			require(sha3(tickets[ticketID].ownerHash)==sha3("")); // seriously?

			tickets[ticketID].owner = msg.sender;
			PurchaseTicket(ticketID,msg.sender);

        }

        function getTickets()public view returns(uint[],uint[],uint[],bool[]){
			uint[]memory identifiers=new uint[](numTickets);
			uint[]memory prices=new uint[](numTickets);
			uint[]memory numbers=new uint[](numTickets);
			bool[]memory isSold=new bool[](numTickets);

			for(uint i=0;i<numTickets; i++){
				identifiers[i]=tickets[i].identifier;
				prices[i]=tickets[i].price;
				numbers[i]=tickets[i].number;
				isSold[i]=tickets[i].isSold;
			}

			return(identifiers,prices,numbers,isSold);
        }

        function getTicket(uint ticketID) public view returns(uint,uint,uint,bool){
			return(tickets[ticketID].identifier,tickets[ticketID].price,tickets[ticketID].number,tickets[ticketID].isSold);
        }


        function getTicketHash(uint ticketID) public view returns(string){
			return tickets[ticketID].ownerHash;
        }
		
						
		function getPurchasedTicket() public view returns(uint,uint){
				return(purchasedTickets[msg.sender].identifier,purchasedTickets[msg.sender].price);
        }

    }